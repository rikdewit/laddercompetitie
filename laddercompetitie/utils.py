from django.utils.text import slugify
import logging


def get_unique_slug(model_instance, slugable_field_name, slug_field_name):
    """
    Takes a model instance, sluggable field name (such as 'title') of that
    model as string, slug field name (such as 'slug') of the model as string;
    returns a unique slug as string.
    """
    slug = slugify(getattr(model_instance, slugable_field_name))
    unique_slug = slug
    extension = 1
    ModelClass = model_instance.__class__

    while ModelClass._default_manager.filter(
        **{slug_field_name: unique_slug}
    ).exists():
        unique_slug = '{}-{}'.format(slug, extension)
        extension += 1

    return unique_slug


def rank_shuffle(ladder):
    best_rank = 1
    for membership in ladder.membership_set.filter(ladder=ladder):
        if membership.rank > best_rank:
            membership.rank = best_rank


            membership.save()
        best_rank = membership.rank + 1
