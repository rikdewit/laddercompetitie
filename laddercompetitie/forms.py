from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser, Challenge

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field


class ChallengeCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['challenged'].queryset = CustomUser.objects.exclude(id=user.id)
    class Meta:
        model = Challenge
        fields = ['challenged','ladder',]


class CustomUserCreationForm(UserCreationForm):
    voornaam = forms.CharField(max_length=30, required=False, help_text='Optional.')
    tussenvoegsel = forms.CharField(max_length=30, required=False, help_text='Optional.')
    achternaam = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=75,required=True)

    class Meta:
        model = CustomUser
        fields = ('username','email')


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('username', 'email')


class LoginForm(forms.Form):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.add_input(Submit('login', 'login', css_class='col-xs-4 col-sm-4 col-md-4 col-lg-4'))
    helper.form_class = 'form-horizontal'
    helper.label_class = 'col-xs-4 col-sm-3 col-md-3 col-lg-3'
    helper.field_class = 'col-xs-8 sol-sm-5 col-md-5 col-lg-5'

    username = forms.CharField(
        label='Gebruikersnaam:',
        required=True
    )

    password = forms.CharField(
        label='Wachtwoord:',
        required=True,
        widget=forms.PasswordInput
    )
