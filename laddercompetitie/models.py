from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.utils.text import slugify

from ckeditor_uploader.fields import RichTextUploadingField
from uuid import uuid4
import os

from .utils import get_unique_slug

from django.utils import timezone


class CustomUser(AbstractUser):
    is_staff = models.BooleanField(
        'administrator',
        default=False,
        help_text='Administratie rechten'
    )

    is_superuser = models.BooleanField(
        # This field is necessary for using ckeditor
        default=False,
    )

    voornaam = models.CharField(
        'voornaam',
        max_length=50,
        blank=True,
        null=True,
        editable=True
    )
    tussenvoegsel = models.CharField(
        'tussenvoegsel',
        max_length=50,
        blank=True,
        null=True,
        editable=True
    )
    achternaam = models.CharField(
        'achternaam',
        max_length=100,
        blank=True,
        null=True,
        editable=True
    )
    geslacht = models.CharField(
        'geslacht',
        max_length=1,
        blank=True,
        null=True,
        editable=True
    )
    geboortedatum = models.DateField(
        'geboortedatum',
        blank=True,
        null=True,
        editable=True
    )
    telefoonnummer = models.CharField(
        'telefoonnummer',
        max_length=15,
        blank=True,
        null=True,
        editable=True
    )

    USERNAME_FIELD  = 'username'

    def naam(self):
        naam = []
        if self.voornaam:
            naam.append(self.voornaam)
        else:
            naam.append(self.voorletters)
        if self.tussenvoegsel:
            naam.append(self.tussenvoegsel)

        naam.append(self.achternaam)

        return ' '.join(naam)
    naam.short_description = 'naam'

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'


class Ladder(models.Model):

    name = models.CharField(
        'naam',
        max_length=128,
    )
    slug = models.SlugField(max_length=50, unique=True, blank=False)

    created_at = models.DateField(default=timezone.now)

    members = models.ManyToManyField(
        CustomUser,
        through='Membership')

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = get_unique_slug(self, 'name', 'slug')
        super().save(*args, **kwargs)


    def __str__(self):
        return self.name

class Membership(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    ladder = models.ForeignKey(Ladder, on_delete=models.CASCADE)
    date_joined = models.DateField(auto_now_add=True)
    rank = models.IntegerField()

    class Meta:
        ordering = ('rank',)

    def __str__(self):
        return "{}{}, {}".format(self.rank,self.user,self.ladder)


class Challenge(models.Model):
    challenger = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name="user_challenger")
    challenged = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name="user_challenged")
    ladder = models.ForeignKey(Ladder, on_delete=models.CASCADE)
    accepted = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
    score = models.CharField(max_length=100,blank=True)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.challenger} vs {self.challenged}, {self.ladder}"
