# Generated by Django 2.1.7 on 2019-04-22 00:22

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('laddercompetitie', '0007_auto_20190422_0212'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ladder',
            name='created_at',
            field=models.DateField(default=datetime.datetime(2019, 4, 22, 0, 22, 39, 403815, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='membership',
            name='date_joined',
            field=models.DateField(default=datetime.datetime(2019, 4, 22, 0, 22, 39, 404614, tzinfo=utc)),
        ),
    ]
