# Generated by Django 2.1.7 on 2019-04-22 01:06

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('laddercompetitie', '0008_auto_20190422_0222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ladder',
            name='created_at',
            field=models.DateField(default=datetime.datetime(2019, 4, 22, 1, 6, 39, 960278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='membership',
            name='date_joined',
            field=models.DateField(default=datetime.datetime(2019, 4, 22, 1, 6, 39, 961038, tzinfo=utc)),
        ),
    ]
