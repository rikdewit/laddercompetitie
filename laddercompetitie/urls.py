from django.contrib import admin
from django.urls import path, include
from .views import (SignupView,
    LadderListView, LadderDetailView, LadderUpdateView, LadderCreateView, LadderDeleteView,
    LadderRegisterView, LadderDeregisterView,
    ChallengeListView, ChallengeCreateView, ChallengeUpdateView, ChallengeDeleteView)

urlpatterns = [

    # Login/logout
    # path('login/',LoginView.as_view()),
    # path('logout/',logout_view,name='logout'),
    path('users/signup/', SignupView.as_view(), name='signup'),
    path('users/', include('django.contrib.auth.urls')),
    #index
    path('', LadderListView.as_view(), name='ladder-list'),
    path('ladder/create/', LadderCreateView.as_view(), name='ladder-create'),
    path('ladder/<slug:slug>/', LadderDetailView.as_view(), name='ladder-detail'),
    path('ladder/<slug:slug>/edit', LadderUpdateView.as_view(), name='ladder-edit'),
    path('ladder/<slug:slug>/delete', LadderDeleteView.as_view(), name='ladder-delete'),
    path('ladder/<slug:slug>/register', LadderRegisterView.as_view(), name='ladder-register'),
    path('ladder/<slug:slug>/deregister', LadderDeregisterView.as_view(), name='ladder-deregister'),

    path('challenges/', ChallengeListView.as_view(), name='challenge-list'),
    path('challenges/create', ChallengeCreateView.as_view(), name='challenge-create'),
    path('challenges/<int:pk>/edit', ChallengeUpdateView.as_view(), name='challenge-edit'),
    path('challenges/<int:pk>/delete', ChallengeDeleteView.as_view(), name='challenge-delete'),


]
