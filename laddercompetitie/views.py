from django.shortcuts import render, redirect
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import get_object_or_404, redirect
from django.core.exceptions import PermissionDenied

from datetime import datetime

from .models import Ladder, CustomUser, Membership, Challenge
from .forms import LoginForm

from django.urls import reverse_lazy, reverse, resolve
from django.views import generic
from .forms import CustomUserCreationForm, ChallengeCreateForm
from django.contrib import messages

from .utils import rank_shuffle
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
import logging






class LadderListView(ListView):
    model = Ladder
    queryset = Ladder.objects.order_by('-created_at','-id')
    template_name = 'laddercompetitie/index.html'
    paginate_by = 6

class LadderDetailView(DetailView):
    model = Ladder
    template_name = "laddercompetitie/ladder_detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        slug = self.kwargs["slug"]
        ladder = Ladder.objects.get(slug=slug)
        players = ladder.members.all().order_by('membership')

        context["players"] = players

        user = self.request.user

        if user.is_authenticated:
            registered = Membership.objects.filter(user=user,ladder=ladder).exists()
        else:
            registered = False

        context["registered"] = registered

        return context


class LadderCreateView(PermissionRequiredMixin, CreateView):
    model = Ladder
    fields = ["name",]
    template_name = 'laddercompetitie/ladder_create.html'

    permission_required = ('ladder.can_create')

    def get_success_url(self):
        return reverse('ladder-detail', kwargs={'slug': self.object.slug})

class LadderUpdateView(PermissionRequiredMixin, UpdateView):
    model = Ladder
    fields= ["name",]
    template_name = 'laddercompetitie/ladder_edit.html'

    permission_required = ('ladder.can_change')

    def get_success_url(self):
        return reverse('ladder-detail', kwargs={'slug': self.object.slug})

class LadderDeleteView(PermissionRequiredMixin, DeleteView):
    model = Ladder
    template_name = 'laddercompetitie/ladder_delete.html'
    success_url = reverse_lazy('ladder-list')

    permission_required = ('ladder.can_delete')

class LadderRegisterView(LoginRequiredMixin, View):
    template_name = "laddercompetitie/ladder_register.html"
    login_url = reverse_lazy('login')

    def get(self, *args, **kwargs):
        context = {"ladder":Ladder.objects.get(slug=self.kwargs["slug"])}
        return render(self.request,self.template_name, context)

    def post(self,*args,**kwargs):
        registered = Membership.objects.filter(user=self.request.user,ladder=Ladder.objects.get(slug=self.kwargs["slug"])).exists()
        if not registered:
            membership = Membership(
                user=self.request.user,
                ladder=Ladder.objects.get(slug=self.kwargs["slug"]),
                rank=self.assign_rank())
            membership.save()
            messages.add_message(self.request, messages.INFO, 'You are registered')
        else:
            messages.add_message(self.request, messages.INFO, 'Already registered')
        return redirect('ladder-detail',slug=self.kwargs["slug"])

    def assign_rank(self):
        ladder = Ladder.objects.get(slug=self.kwargs["slug"])
        players = ladder.members.all()

        worst_rank = 0
        for player in players:
            rank = Membership.objects.get(ladder=ladder, user=player).rank
            if rank >= worst_rank:
                worst_rank = rank
        return worst_rank + 1


class LadderDeregisterView(LoginRequiredMixin, View):
    template_name="laddercompetitie/ladder_deregister.html"

    def get(self, *args, **kwargs):
        context = {"ladder":Ladder.objects.get(slug=self.kwargs["slug"])}
        return render(self.request,self.template_name, context)

    def post(self, *args, **kwargs):
        registered = Membership.objects.filter(user=self.request.user,ladder=Ladder.objects.get(slug=self.kwargs["slug"])).exists()
        if registered:
            slug = slug=self.kwargs["slug"]
            ladder = Ladder.objects.get(slug=slug)
            user=self.request.user
            user.membership_set.get(ladder=ladder).delete()
            messages.add_message(self.request, messages.INFO, 'successfully deregistered')
            rank_shuffle(ladder)
        else:
            messages.add_message(self.request, messages.INFO, 'Not registered')
        return redirect('ladder-detail',slug=slug)

class ChallengeListView(LoginRequiredMixin, ListView):
    template_name="laddercompetitie/challenge_list.html"
    model=Challenge

    def get_queryset(self):
        q1 = Challenge.objects.filter(challenger=self.request.user)
        q2 = Challenge.objects.filter(challenged=self.request.user)
        return q1.union(q2).order_by('accepted')

class ChallengeCreateView(LoginRequiredMixin, CreateView):
    model = Challenge
    #fields = ["challenged","ladder"]
    template_name = 'laddercompetitie/challenge_create.html'
    success_url = reverse_lazy('challenge-list')
    form_class = ChallengeCreateForm

    def form_valid(self, form):
        challenger = CustomUser.objects.get(pk=self.request.user.pk)
        form.instance.challenger = challenger
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class ChallengeUpdateView(LoginRequiredMixin, UpdateView):
    model = Challenge
    template_name = 'laddercompetitie/challenge_edit.html'
    success_url = reverse_lazy('challenge-list')
    fields = ["accepted",]

    def get_object(self, *args, **kwargs):
        object = super().get_object(*args, **kwargs)
        if object.challenged != self.request.user:
            raise PermissionDenied()

        if object.accepted:
            raise PermissionDenied()

        return object



class ChallengeDeleteView(LoginRequiredMixin, DeleteView):
    model = Challenge
    template_name = 'laddercompetitie/challenge_delete.html'
    success_url = reverse_lazy('challenge-list')

    def get_object(self, *args, **kwargs):
        object = super().get_object(*args, **kwargs)
        if object.challenged != self.request.user and object.challenger != self.request.user:
            raise PermissionDenied()
        return object




class SignupView(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'



# class LoginView(View):
#     def get(self, request, *args, **kwargs):
#         context = {
#             'loginform': LoginForm(),
#         }
#         return render(request, 'laddercompetitie/login.html', context)
#
#     def post(self, request, *args, **kwargs):
#         loginform = LoginForm(request.POST)
#
#         if loginform.is_valid():
#             username = loginform.cleaned_data['username']
#             password = loginform.cleaned_data['password']
#             user = authenticate(request,username=username,password=password)
#
#             if user is not None:
#                 # Succesfull login
#                 login(request,user)
#                 return redirect('/')
#             else:
#                 error_message = 'Username and/or password incorrect!'
#         else:
#             error_message = 'Invalid form!'
#
#         context = {
#             'loginform': loginform,
#             'error_message': error_message,
#         }
#         return render(request, 'app/login.html', context)
#
#
# def logout_view(request):
#     logout(request)
#     return redirect('/')
