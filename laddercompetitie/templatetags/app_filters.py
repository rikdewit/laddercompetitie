from django import template
from ..models import Ladder, CustomUser, Membership

register = template.Library()
@register.filter(name="rank")
def rank(user, ladder):
    return Membership.objects.get(ladder=ladder, user=user).rank
