DEBUG = True

CRISPY_FAIL_SILENTLY = False

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
   }
}
