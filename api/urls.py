from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from api import views

router = routers.DefaultRouter()
router.register(r'ladders',views.LadderViewSet)
router.register(r'users',views.CustomUserViewSet)


urlpatterns = [
    path('profile', views.ProfileView.as_view(),name='profile'),
    path('',include(router.urls)),
]
