from laddercompetitie.models import CustomUser, Ladder
from .serializers import LadderSerializer, CustomUserSerializer, ProfileSerializer
from django.http import Http404
from rest_framework import viewsets
from rest_framework import generics, mixins, permissions
from rest_framework.response import Response
from rest_framework import status

class LadderViewSet(viewsets.ModelViewSet):
    queryset = Ladder.objects.all()
    serializer_class = LadderSerializer

class CustomUserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer


class UserIsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.id == request.user.id

class ProfileView(generics.RetrieveUpdateDestroyAPIView):

    serializer_class = ProfileSerializer

    def get_object(self):
      return CustomUser.objects.get(id=self.request.user.id)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    # def update(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     instance.voornaam = request.data.get("voornaam")
    #     instance.save()
    #
    #     serializer = self.get_serializer(instance)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_update(self, serializer)
    #
    #     return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)




# class LadderDetail(APIView):
#     def get_object(self, pk):
#         try:
#             return Ladder.objects.get(pk=pk)
#         except Ladder.DoesNotExist:
#             raise Http404
#
#     def get(self, request, pk, format=None):
#         ladder = self.get_object(pk)
#         serializer = LadderSerializer(ladder)
#         return Response(serializer.data)
#
#     def put(self, request, pk, format=None):
#         ladder = self.get_object(pk)
#         serializer = LadderSerializer(ladder, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request, pk, format=None):
#         ladder = self.get_object(pk)
#         ladder.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)
