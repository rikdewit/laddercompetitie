from laddercompetitie.models import CustomUser, Ladder
from rest_framework import serializers

class LadderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ladder
        fields = ('url','name','created_at','members')

class CustomUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = ('url','username', 'email','voornaam','tussenvoegsel','achternaam')

class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = ('url','username', 'email','voornaam','tussenvoegsel','achternaam')
