docker kill $(docker ps -q)
docker rm -v $(docker ps -a -q -f name="db_laddercompetitie")
docker-compose run web python manage.py migrate --noinput

#create superuser
docker-compose run web python manage.py loaddata test.json

docker rm -v $(docker ps -a -q -f status=exited)

sudo chown -R $USER:$USER .
